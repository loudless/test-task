'use strict';

var webpack = require('webpack');

const NODE_ENV = process.env.NODE_ENV || 'dev';

module.exports = {
  entry: './index.js',

  output: {
    filename: 'bundle.js',
    publicPath: '',
    path: __dirname + '/public'
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader?presets[]=es2015&presets[]=react'
      }
    ]
  },

  plugins: [
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV)
    }),
  ]
}

if (NODE_ENV == 'prod') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      },
      output: {
        comments: false
      }
    })
  );
}
