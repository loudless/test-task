import getOr from 'lodash/fp/getOr';

export default function (state = [], action) {
  switch (action.type) {
    case 'FLIGHTS_LOADED':
      return getOr([], 'data.flights', action);
    default:
      return state;
  }
}
