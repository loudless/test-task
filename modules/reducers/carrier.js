export default function (state = [], action) {
  switch (action.type) {
    case 'SELECT_CARRIER':
      return action.carrier
    default:
      return state;
  }
}
