import uniq from 'lodash/fp/uniq';
import map from 'lodash/fp/map';
import compose from 'lodash/fp/compose';
import filter from 'lodash/fp/filter';

const getUniqCarriers = compose(uniq, map('carrier'));

const filterFlights = function filterFlights (flights, carrier) {
  if (carrier && carrier !== '*') {
    return filter({carrier: carrier}, flights);
  } else {
    return flights
  }
};

export { getUniqCarriers, filterFlights };
