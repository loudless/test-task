import { createStore, combineReducers, applyMiddleware } from 'redux';
import promiseMiddleware from './middlewares/promise';

import flights from './reducers/flights';
import carrier from './reducers/carrier';

let middleware = [promiseMiddleware];
if (NODE_ENV !== 'prod') {
  const createLogger = require('redux-logger');
  const logger = createLogger();

  middleware = [...middleware, logger];
}

const reducer = combineReducers({
  flights,
  carrier,
});

const store = createStore(reducer, {
    flights: [],
    carrier: null,
  },
  applyMiddleware(...middleware),
);

export default store;
