import { getFlightsData } from './api';

export function loadFlightsData() {
  return {
    type: 'PROMISE',
    actions: ['FLIGHTS_LOADING', 'FLIGHTS_LOADED', 'FLIGHTS_LOAD_FAILURE'],
    promise: getFlightsData(),
  };
}

export function selectCarrier(carrier) {
  return {
    type: 'SELECT_CARRIER',
    carrier,
  }
}
