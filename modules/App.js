import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import React, { Component } from 'react';

import FlightsList from './components/FlightsList';
import CarrierSelect from './components/CarrierSelect';
import { loadFlightsData } from './actions';
import { filterFlights, getUniqCarriers } from './utils';

class App extends Component {
  componentDidMount() {
    this.props.loadFlightsData();
  }

  render() {
    const flights = this.props.flights;
    const carriers = getUniqCarriers(flights);
    const filteredFlights = filterFlights(flights, this.props.carrier);

    return (
      <div>
        <h1>OneTwoTrip Test Task</h1>
        <CarrierSelect carriers={carriers}/>
        <FlightsList flights={filteredFlights}/>
      </div>
    )
  }
}

export default connect(
  (state) => {
    return {
      carrier: state.carrier,
      flights: state.flights,
    }
  },
  (dispatch) => bindActionCreators({loadFlightsData}, dispatch),
)(App);
