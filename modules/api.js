import 'whatwg-fetch';
import store from './store';
import getOr from 'lodash/fp/getOr';

const FLIGHTS_GET_URL = '/data.json';

export function getFlightsData() {
  return fetch(FLIGHTS_GET_URL)
    .then(res => res.json())
    .then(data => {
      const flights = getOr([], 'flights', data);

      return {
        flights,
      }
    });
}
