import React, {Component} from 'react';
import FlightCard from './FlightCard';

import map from 'lodash/fp/map';

const createFlightCard = (flight) => <FlightCard key={flight.id} flight={flight} />;

export default class FlightsList extends Component {
  render() {
    return (
      <div>
        { map(createFlightCard, this.props.flights) }
      </div>
    )
  }
}
