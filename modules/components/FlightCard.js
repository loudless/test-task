import React, {Component} from 'react';
import moment from 'moment';

const parseDateTime = (dateTime) => moment.parseZone(dateTime).format('LLL');

export default class FlightCard extends Component {
  render() {
    return (
      <div className="card">
        <div className="container">
          <p><b>Flight:</b> {this.props.flight.direction.from} — {this.props.flight.direction.to}</p>
          <p><b>Departure:</b> {parseDateTime(this.props.flight.departure)}</p>
          <p><b>Arrival:</b> {parseDateTime(this.props.flight.arrival)}</p>
          <p><b>Carrier:</b> {this.props.flight.carrier}</p>
        </div>
      </div>
    )
  }
}
