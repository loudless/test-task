import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import React, {Component} from 'react';

import {selectCarrier} from '../actions';

import bind from 'lodash/fp/bind';
import map from 'lodash/fp/map';
const mapWithIndex = map.convert({ 'cap': false });

const createSelectOption = (carrier, index) => <option key={index}>{carrier}</option>;

class CarrierSelect extends Component {
  handleChange(event) {
    this.props.selectCarrier(event.target.value);
  }

  render() {
    const carriers = this.props.carriers;

    return (
      <div>
        <select className="selectbox" onChange={bind(this.handleChange, this)}>
          <option value="*">All carriers</option>
          { mapWithIndex(createSelectOption, carriers) }
        </select>
      </div>
    )
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => bindActionCreators({selectCarrier}, dispatch),
)(CarrierSelect);
